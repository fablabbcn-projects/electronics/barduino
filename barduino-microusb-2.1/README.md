# Barduino 2.1

![](img/Barduino2.1.jpg)

Improvements:
- added microUsb connector
- added embedded ftdi communication
- smaller footprint ~50%smaller

This board integrates the FTDI connection allowing the user to connect the board throughout a microUSB cable.

![](../img/Barduino2.1.jpg)

**FootPrints**

Traces-1
![](fab-png/Barduino2.1-Traces.png)

Traces-2 *Required to mill the copper under the antenna*
![](fab-png/Barduino2.1-Antena.png)

Interior-Holes
![](fab-png/Barduino2.1-Holes.png)

Interior-Outcut
![](fab-png/Barduino2.1-Outline.png)

**Downloads** *Right click - save as*

- [Kicad Sch](BarDuino2.1-KicadFiles/Barduino2.1.sch)
- [Kicad brd](BarDuino2.1-KicadFiles/Barduino2.1.pro)
- [Kicad libraries ESP32](BarDuino2.1-KicadFiles/Barduino2.1.kicad_pcb)