# Barduino 3.0

This is a development board for an ESP32 microcontroller together with an ATSAMD11C14A. The main idea of this project is to use the SAMD11 as a Serial bridge to program the ESP32, but beeing able to program boths microcontrollers in a single board with the usable pins open for prototypeing.

![](../img/Barduino3.PNG)

![](../img/Barduino3Hero.jpg)

Improvements:
- ESP32Wroom-DA with double antenna
- SAMD11C14A instead of FT230XS for make the soldering process easier
- sized 72 x 50 mm like the PCB boards to avoid cutting the outline
- test led on the ESP32 Gpio13 
- test led on the SAMD11 PA02

**Fabrication files**

Fabbable files are found under: [barduino-microusb-3.0/BarDuino3.0-KicadFiles/fab-png](barduino-microusb-3.0/BarDuino3.0-KicadFiles/fab-png)

## Programming

The Barduino 3.0 keeps being 100% Arduino IDE and libraries compatible, fabbable and open source board based on Neil´s work on the Esp32 board, but in ths case it also has the SAMD11C14A that needs to be programmed. This board is base in the [Bridge Serial D11C](https://gitlab.fabcloud.org/pub/programmers/programmer-serial-d11c) developed by Krisjanis Rijnieks. Follow the board documentation to program it as a Serial bridge.

### Arduino IDE

To set up the arduino IDE follow this steps: 

1. In the Preferences Menu (File/ Preferences) setup the *Additional Boards Manager URLs* with this 2 URLs:

```
https://dl.espressif.com/dl/package_esp32_index.json

https://raw.githubusercontent.com/qbolsee/ArduinoCore-fab-sam/master/json/package_Fab_SAM_index.json

```

2. Under Tools/Boards/Boards Manager install the *esp32* by Espressif Systems and the *Fab SAM core for Arduino* by Fab Foundation.

#### Programming the SAMD11

Please, find the original documentation [here](https://gitlab.fabcloud.org/pub/programmers/programmer-serial-d11c).

##### Uploading the bootloader

To be able to program the SAMD11 from the Arduino IDE, it should be programmed beforehand with a bootloader. To do so, a [SWD programmer](https://gitlab.fabcloud.org/pub/programmers/programmer-swd-d11c) or an ATMEL ICE is needed. 

To burn the bootloader EDBG can be used. 

1. Install [EDBG](https://github.com/ataradov/edbg) from [here](https://taradov.com/bin/edbg/).

2. Download the sam BA bootloader from the [Fab SAM core](https://github.com/qbolsee/ArduinoCore-fab-sam). You can download the binary for the bootloader [here](https://github.com/qbolsee/ArduinoCore-fab-sam/blob/master/bootloaders/zero/binaries/sam_ba_SAMD11C14A.bin?raw=true).

3. With the programmer connected to the board and both connected to the computer use the following command to upload the bootloader using edbg:

```
edbg -ebpv -t samd11 -f sam_ba_SAMD11C14A.bin
```

Once the bootloader is burned the board should appear in the Port's list in Arduino IDE. 

##### Programming the SAMD11 as a Serial Bridge

With the bootloader already burned, the SAMD11 can be programmed using the Arduino IDE. The board has a Built in LED in Pin number 2 (PA02) of the SAMD11 for testing purposes.

To use the SAMD11 to program the ESP32, the SAMD should be programmed as a Serial Bridge, to establish the connection between the USB and the ESP32. To do so, define the Serial Config (Tools menu in the Arduino IDE) as *TWO UART NO WIRE NO SPI* and uplode [this code](https://github.com/qbolsee/SAMD11C_serial/blob/main/SAMD11C_serial/SAMD11C_serial.ino) by Quentin Bolsee. 

##### Programming the ESP32

With the SAMD11 programmed as a Serial Bridge, the ESP32 can be flashed using the Board *ESP32 Dev Module* and selecting the port of the SAMD11. Do not forget to set the slide switch to the left to boot in flash mode and press the reset button before uploading the code. Once the code is uploaded, slide the switch to the right side and reset the ESP32. For testing purposes, it has a Built in LED in the GPIO13.


## B.O.M

Complete BOM is found under: [barduino-microusb-3.0/BarDuino3.0-KicadFiles/exports/BOM-Barduino3.0.csv](barduino-microusb-3.0/BarDuino3.0-KicadFiles/exports/BOM-Barduino3.0.csv)

### License

This work is license under CERN OHL V1.2:
https://ohwr.org/project/licences/wikis/CERN-OHL-v1.2
